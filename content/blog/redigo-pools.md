+++
date = "2016-05-20T15:47:24+04:00"
tags=["Go", "golang", "redis", "redigo", "pools"]
title = "Golang - redigo pools"
categories = ["Golang"]
+++
```
newPool() *redis.Pool {
  return &redis.Pool{
    MaxIdle:   80,
    MaxActive: 12000, // max number of connections
    Dial: func() (redis.Conn, error) {
      c, err := redis.Dial("tcp", ":6379")
      if err != nil {
        panic(err.Error())
      }
      return c, err
    },
  }

}

var pool = newPool()

func main() {

  c := pool.Get()
  defer c.Close()

  test, _ := c.Do("HGETALL", "test:1")
  fmt.Println(test)
}
```
